MODULE_NAME = sshync

.PHONY: build
build:
	go build -v -o $(MODULE_NAME) main.go

.PHONY: run
run:
	go run main.go
