# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.7] - 2020-12-21

### Fixed

- Download links for binaries

## [0.1.6] - 2020-12-21

### Added

- CI pipeline for `aarch64` and canonical naming scheme for binaries

## [0.1.5] - 2020-04-03

### Added

- CI pipeline
- Automated tagging via [autotag](https://gitlab.com/nicklasfrahm/autotag)
- Cross-compiled binaries for `arm`, `386` and `amd64`

### Fixed

- Directory permissions of `.ssh` directory and `authorized_keys` files

## [0.1.4] - 2019-10-21

### Fixed

- Directory permissions of configuration directory `/etc/sshync` are now `0755`

## [0.1.3] - 2019-10-16

### Added

- Configuration option `files` to synchronize keys to arbitrary files
- Configuration option `hooks` to execute arbitrary commands after updating the SSH keys

## [0.1.2] - 2019-10-16

### Fixed

- Fix version number in command line help text
- Update help text in `README.md`
- Fix version number in `User-Agent` HTTP header
- Keep existing SSH keys if JSON cannot be parsed
- Improve error messages

## [0.1.1] - 2019-08-11

### Fixed

- Only create `authorized_keys.old` if `authorized_keys` previously existed and was not empty
- Improve efficiency by using file metadata instead of reading entire file into memory

## [0.1.0] - 2019-08-08

### Added

- `sshync daemon` command to periodically synchronize SSH keys
- `sshync install` command to install application as systemd service
- `sshync uninstall` command to uninstall application as systemd service
