package keys

import (
	"fmt"
	"io"
	"os"
	"os/user"
	"strconv"
	"strings"
)

// Write writes SSH keys to the users ".ssh/authorized_keys" file.
//
// The function will ensure that a backup of the existing
// "authorized_keys" exists at ".ssh/authorized_keys.bak".
// Afterwards it will write the keys passed to it.
func Write(keyFilePath string, owner string, keys []string) error {
	// Define other folder and file locations.
	keyDirSegments := strings.Split(keyFilePath, "/")
	keyDir := strings.Join(keyDirSegments[:len(keyDirSegments)-1], "/")
	backupFilePath := fmt.Sprintf("%s.old", keyFilePath)

	// Ensure that the ".ssh" directory exists.
	if err := os.MkdirAll(keyDir, 0700); err != nil {
		return err
	}

	// Set correct permissions for ".ssh" directory.
	if err := Chown(keyDir, owner); err != nil {
		return err
	}

	// Open "authorized_keys" file.
	keyFile, err := os.OpenFile(keyFilePath, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}

	// Read metainformation of "authorized_keys" file.
	keyFileInfo, err := keyFile.Stat()
	if err != nil {
		return err
	}

	// If the "authorized_keys" file was not previously empty, create a backup.
	if keyFileInfo.Size() != 0 {
		// Ensure that a backup of the previous "authorized_keys" file exists.
		backupFile, err := os.OpenFile(backupFilePath, os.O_RDWR|os.O_CREATE, 0644)
		if err != nil {
			return err
		}

		// Create backup if backup file is empty.
		backupFileInfo, err := backupFile.Stat()
		if err != nil {
			return err
		}
		if backupFileInfo.Size() == 0 {
			// Copy file content from key file to backup file.
			if _, err = io.Copy(backupFile, keyFile); err != nil {
				return err
			}
		}

		// Close backup file.
		if err = backupFile.Close(); err != nil {
			return err
		}

		// Set correct permissions for backup file.
		if err := Chown(backupFilePath, owner); err != nil {
			return err
		}

		// Clean file before next write and set cursor to beginning of file.
		if err = keyFile.Truncate(0); err != nil {
			return err
		}
		if _, err = keyFile.Seek(0, 0); err != nil {
			return err
		}
	}

	// Create key file content by separating SSH keys with Unix style line endings.
	keyFileContent := fmt.Sprintln(strings.Join(keys, "\n"))
	_, err = keyFile.WriteString(keyFileContent)
	if err != nil {
		return err
	}

	// Close key file.
	err = keyFile.Close()
	if err != nil {
		return err
	}

	// Set correct permissions for key file.
	if err := Chown(keyFilePath, owner); err != nil {
		return err
	}

	return nil
}

// Chown gives ownership to a file or folder to the specified user.
func Chown(file string, ownerName string) error {
	// Lookup user to find user and group ID.
	newOwner, err := user.Lookup(ownerName)
	if err != nil {
		return err
	}

	// Convert user and group ID to integer.
	uid, err := strconv.Atoi(newOwner.Uid)
	if err != nil {
		return err
	}
	gid, err := strconv.Atoi(newOwner.Gid)
	if err != nil {
		return err
	}

	// Grant ownership.
	if err := os.Chown(file, uid, gid); err != nil {
		return err
	}

	return nil
}
