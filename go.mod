module gitlab.com/nicklasfrahm/sshync

go 1.11

require (
	github.com/urfave/cli v1.22.1
	gopkg.in/yaml.v3 v3.0.0-20191010095647-fc94e3f71652
)
