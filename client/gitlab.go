package client

import (
	"encoding/json"
	"fmt"
	"strings"
)

type gitlabUser struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	State     string `json:"state"`
	AvatarURL string `json:"avatar_url"`
	WebURL    string `json:"web_url"`
}

type gitlabUserKey struct {
	ID        int64  `json:"id"`
	Title     string `json:"title"`
	Key       string `json:"key"`
	CreatedAt string `json:"created_at"`
}

// GetGitlabUserID translates the GitLab username into a GitLab user ID.
func GetGitlabUserID(username string) (int64, error) {
	// Request user keys.
	res, err := NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/users?username=%s", username), nil)
	if err != nil {
		return -1, err
	}

	// Parse response body.
	var users []gitlabUser
	err = json.Unmarshal([]byte(res), &users)
	if err != nil {
		return -1, fmt.Errorf("Parsing JSON failed: %s", err.Error())
	}

	// Retrieve IDs from response array.
	ids := []int64{}
	for _, user := range users {
		ids = append(ids, user.ID)
	}

	if len(ids) == 0 {
		return -1, fmt.Errorf("Unknown user: %s", username)
	}

	return ids[0], nil
}

// GetGitlabUserKeys returns the SSH keys of a GitLab user.
func GetGitlabUserKeys(username string) ([]string, error) {
	// Translate GitLab username to user ID.
	userID, err := GetGitlabUserID(username)
	if err != nil {
		return nil, err
	}

	// Request user keys.
	res, err := NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/users/%d/keys", userID), nil)
	if err != nil {
		return nil, err
	}

	// Parse response body.
	userKeys := []gitlabUserKey{}
	err = json.Unmarshal([]byte(res), &userKeys)
	if err != nil {
		return nil, fmt.Errorf("Parsing JSON failed: %s", err.Error())
	}

	// Retrieve keys from response array.
	keys := []string{}
	for _, userKey := range userKeys {
		// Remove description from SSH key.
		key := strings.Join(strings.Split(userKey.Key, " ")[:2], " ")
		keys = append(keys, key)
	}

	return keys, nil
}
