package client

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func handleError(err error, method string, url string) ([]byte, error) {
	// Log to console.
	log.Printf("Error: Request failed: %s %s\n", method, url)
	return nil, err
}

// NewRequest creates a new HTTP client.
func NewRequest(method string, url string, payload io.Reader) ([]byte, error) {
	// Create new HTTP client.
	client := &http.Client{}

	// Configure HTTP request with headers.
	req, err := http.NewRequest(method, url, payload)
	req.Header.Set("User-Agent", "sshync/0.1.4")
	if err != nil {
		return handleError(err, method, url)
	}

	// Receive HTTP response.
	res, err := client.Do(req)
	if err != nil {
		return handleError(err, method, url)
	}

	// Read response body.
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return handleError(err, method, url)
	}

	// Close response body.
	err = res.Body.Close()
	if err != nil {
		return handleError(err, method, url)
	}

	return body, nil
}
