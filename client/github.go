package client

import (
	"encoding/json"
	"fmt"
)

type gitHubUserKey struct {
	ID  int64  `json:"id"`
	Key string `json:"key"`
}

// GetGithubUserKeys returns the SSH keys of a GitHub user.
func GetGithubUserKeys(username string) ([]string, error) {
	// Request user keys.
	res, err := NewRequest("GET", fmt.Sprintf("https://api.github.com/users/%s/keys", username), nil)
	if err != nil {
		return nil, err
	}

	// Parse response body.
	userKeys := []gitHubUserKey{}
	err = json.Unmarshal([]byte(res), &userKeys)
	if err != nil {
		return nil, fmt.Errorf("Parsing JSON failed: %s", err.Error())
	}

	// Retrieve keys from response array.
	keys := []string{}
	for _, userKey := range userKeys {
		keys = append(keys, userKey.Key)
	}

	return keys, nil
}
