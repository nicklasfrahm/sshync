package cli

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/nicklasfrahm/sshync/daemon"
)

var app = cli.NewApp()

func init() {
	app.Name = "sshync"
	app.Usage = "synchronize SSH keys from GitLab and GitHub to servers"
	app.Author = "nicklasfrahm"
	app.Version = "0.1.4"
	app.Commands = []cli.Command{
		{
			Name:    "daemon",
			Aliases: []string{"d"},
			Usage:   "Run daemon in foreground",
			Action: func(c *cli.Context) error {
				return daemon.Run()
			},
		},
		{
			Name:    "install",
			Aliases: []string{"i"},
			Usage:   "Install daemon to system manager",
			Action: func(c *cli.Context) error {
				return daemon.Install()
			},
		},
		{
			Name:    "uninstall",
			Aliases: []string{"un"},
			Usage:   "Uninstall daemon from system manager",
			Action: func(c *cli.Context) error {
				return daemon.Uninstall()
			},
		},
	}
}

// Run starts the CLI application.
func Run() error {
	// Pass command line arguments to application.
	if err := app.Run(os.Args); err != nil {
		return err
	}

	return nil
}
