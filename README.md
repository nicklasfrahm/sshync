# SSHync

[![Go Report](https://goreportcard.com/badge/gitlab.com/nicklasfrahm/sshync)](https://goreportcard.com/badge/gitlab.com/nicklasfrahm/sshync)

Synchronize SSH keys from GitLab and GitHub to servers.

## Download

The latest builds can be found here:

- [sshync-armv7](https://gitlab.com/nicklasfrahm/sshync/-/jobs/artifacts/master/raw/sshync-armv7?job=compile-armv7)
- [sshync-armv8](https://gitlab.com/nicklasfrahm/sshync/-/jobs/artifacts/master/raw/sshync-armv8?job=compile-armv8)
- [sshync-amd32](https://gitlab.com/nicklasfrahm/sshync/-/jobs/artifacts/master/raw/sshync-amd32?job=compile-amd32)
- [sshync-amd64](https://gitlab.com/nicklasfrahm/sshync/-/jobs/artifacts/master/raw/sshync-amd64?job=compile-amd64)

Please note that these builds are created on a daily basis from the `master` branch. For a specific version, the download link has to be constructed by hand. For reference, please refer to the [GitLab artifact download link documentation](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html#downloading-the-latest-artifacts).

## Usage

```txt
NAME:
   sshync - synchronize SSH keys from GitLab and GitHub to servers

USAGE:
   sshync [global options] command [command options] [arguments...]

VERSION:
   0.1.4

AUTHOR:
   nicklasfrahm

COMMANDS:
     daemon, d      Run daemon in foreground
     install, i     Install daemon to system manager
     uninstall, un  Uninstall daemon from system manager
     help, h        Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

The application can be configured via a config file located at `/etc/sshync/config.yml`:

```yaml
hooks:
  post:
    - update-initramfs -u
credentials:
  - user: ubuntu
    ssh_keys:
      - provider: github
        username: nicklasfrahm
    files:
      - path: /etc/dropbear-initramfs/authorized_keys
        options:
          - command="/bin/cryptroot-unlock"
daemon:
  poll_interval:
    seconds: 300
```

## Development

Make sure to have at least `go1.11` installed as this package uses **Go 1.11 Modules**. You may also want to install `make` if you are on a Linux system.

To build the binary of the application, run `make`.

To simply run the application, run `make run`.

## Disclaimer

This is my first `golang` project, but I am open to improvements and feedback of any kind.

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
