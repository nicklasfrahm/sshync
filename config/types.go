package config

// SSHKey describes the provider and the entity that own the SSH key.
type SSHKey struct {
	Provider string `yaml:"provider"`
	Username string `yaml:"username"`
}

// SSHKeyFile describes additional files to synchronize the SSH keys to.
type SSHKeyFile struct {
	Path    string   `yaml:"path"`
	Options []string `yaml:"options"`
}

// Credential defines the relationship between a user account and SSH keys.
type Credential struct {
	User    string       `yaml:"user"`
	SSHKeys []SSHKey     `yaml:"ssh_keys"`
	Files   []SSHKeyFile `yaml:"files"`
}

// Config defines the configuration of the daemon.
type Config struct {
	Hooks struct {
		Post []string `yaml:"post"`
	} `yaml:"hooks"`
	Credentials []Credential `yaml:"credentials"`
	Daemon      struct {
		PollInterval struct {
			Seconds int `yaml:"seconds"`
		} `yaml:"poll_interval"`
	} `yaml:"daemon"`
}
