package config

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

// Create and cache config object.
var config Config

// Load reads the configuration file of the daemon.
func Load() (*Config, error) {
	// Create configuration directory.
	if err := os.MkdirAll("/etc/sshync/", 0755); err != nil {
		return nil, err
	}

	// Create configuration file if it does not exist.
	yamlFile, err := os.OpenFile("/etc/sshync/config.yaml", os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}

	// Read YAML string from file.
	yamlString, err := ioutil.ReadAll(yamlFile)
	if err != nil {
		return nil, err
	}

	// Parse YAML file based on configuration struct.
	err = yaml.Unmarshal(yamlString, &config)
	if err != nil {
		return nil, err
	}

	// Create default configuration if it did not exist.
	if len(yamlString) == 0 {
		emptyConfig := Config{
			Credentials: []Credential{
				{
					User: "",
					SSHKeys: []SSHKey{
						{
							Provider: "",
							Username: "",
						},
					},
					Files: []SSHKeyFile{
						{
							Path:    "",
							Options: []string{},
						},
					},
				},
			},
		}
		defaultYamlString, _ := yaml.Marshal(&emptyConfig)
		yamlFile.WriteString(string(defaultYamlString))
	}

	// Return pointer to config object.
	return &config, nil
}
