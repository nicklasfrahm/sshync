package daemon

import (
	"errors"
	"io/ioutil"
	"os"
)

// Install will configure the daemon to run in the background via the system manager.
//
// Currently the only supported system manager is systemd.
func Install() error {
	// Check if user has root privileges.
	if err := IsRoot(); err != nil {
		return err
	}

	// Check if system manager is systemd.
	if linkDest, err := os.Readlink("/sbin/init"); linkDest != "/lib/systemd/systemd" {
		return errors.New("System manager has to be one of: systemd")
	} else if err != nil {
		return err
	}

	// Ensure that systemd directory exists.
	if err := os.MkdirAll("/etc/systemd/system/", 0644); err != nil {
		return err
	}

	// Define systemd service.
	serviceDefinition := []byte(`[Unit]
Description=sshync background daemon
Documentation=https://gitlab.com/nicklasfrahm/sshync
After=network.target

[Service]
ExecStart=/bin/sshync daemon
Type=simple
User=root
KillMode=control-group
Restart=always
RestartSec=5
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
`)

	// Write systemd service.
	if err := ioutil.WriteFile("/etc/systemd/system/sshync.service", serviceDefinition, 0644); err != nil {
		return err
	}

	// Enable systemd service on startup.
	if err := Exec("systemctl enable sshync"); err != nil {
		return err
	}

	// Start systemd service.
	if err := Exec("systemctl start sshync"); err != nil {
		return err
	}

	return nil
}
