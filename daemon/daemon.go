package daemon

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/nicklasfrahm/sshync/client"
	"gitlab.com/nicklasfrahm/sshync/config"
	"gitlab.com/nicklasfrahm/sshync/keys"
)

// Daemon fetches SSH keys from provides and update the key files.
func Daemon() error {
	// Load configuration to ensure that latest key configuration is used.
	cfg, err := config.Load()
	if err != nil {
		return err
	}

	// Additional SSH key files.
	fileKeyMap := map[string]map[string]map[string]bool{}

	// Load keys and assign them to the specified user for all users in config.
	for _, credential := range cfg.Credentials {
		// Ensure uniqueness by adding keys to map.
		keyMap := map[string]bool{}

		// Load keys from specified providers.
		for _, sshKey := range credential.SSHKeys {
			providerKeys := []string{}

			// Check if SSH key provider is supported.
			switch sshKey.Provider {
			case "github":
				// Load keys from GitHub.
				providerKeys, err = client.GetGithubUserKeys(sshKey.Username)
				if err != nil {
					return err
				}
			case "gitlab":
				providerKeys, err = client.GetGitlabUserKeys(sshKey.Username)
				if err != nil {
					return err
				}
			case "":
				return fmt.Errorf("SSH key provider must not be empty")
			default:
				return fmt.Errorf("Unsupported SSH key provider: %s", sshKey.Provider)
			}

			// Deduplicate SSH keys.
			for _, publicKey := range providerKeys {
				keyMap[publicKey] = true

				// Deduplicate options and files.
				for _, file := range credential.Files {
					// Ensure that the inner map is initialized.
					if fileKeyMap[file.Path] == nil {
						fileKeyMap[file.Path] = map[string]map[string]bool{}
					}

					for _, option := range file.Options {
						// Ensure that the inner map is initialized.
						if fileKeyMap[file.Path][publicKey] == nil {
							fileKeyMap[file.Path][publicKey] = map[string]bool{}
						}

						fileKeyMap[file.Path][publicKey][option] = true
					}
				}
			}
		}

		// Convert map to simple string slice.
		keyStrings := []string{}
		for key := range keyMap {
			keyStrings = append(keyStrings, key)
		}

		// Write keys to user's "authorized_keys" file.
		if len(credential.User) != 0 {
			err := keys.Write(fmt.Sprintf("/home/%s/.ssh/authorized_keys", credential.User), credential.User, keyStrings)
			if err != nil {
				return err
			}

			// Print statistics.
			log.Printf("Wrote SSH keys for user: %s (%d)\n", credential.User, len(keyStrings))
		}

		// Write keys to additional key files.
		for filePath := range fileKeyMap {
			// Collect all keys for a particular SSH file.
			fileKeyStrings := []string{}

			for key := range fileKeyMap[filePath] {
				// Collect all options for a specific SSH key.
				keyOptions := []string{}

				for option := range fileKeyMap[filePath][key] {
					keyOptions = append(keyOptions, option)
				}

				// Merge SSH key options and SSH key.
				keyOptionsString := strings.Join(keyOptions, ",")
				keyWithOptions := strings.Join([]string{keyOptionsString, key}, " ")

				fileKeyStrings = append(fileKeyStrings, keyWithOptions)
			}

			err := keys.Write(filePath, "root", fileKeyStrings)
			if err != nil {
				return err
			}

			// Print statistics.
			log.Printf("Wrote SSH keys to file: %s (%d)\n", filePath, len(fileKeyStrings))
		}

		// Execute post update hook.
		for _, hook := range cfg.Hooks.Post {
			if err := Exec(hook); err != nil {
				return err
			}
		}
	}

	return nil
}
