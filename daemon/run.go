package daemon

import (
	"log"
	"time"

	"gitlab.com/nicklasfrahm/sshync/config"
)

// Run starts a daemon that will periodically synchronize the SSH keys.
func Run() error {
	// Read and parse configuration file.
	cfg, err := config.Load()
	if err != nil {
		return err
	}

	// Prevent extremely low poll intervals to avoid rate limits.
	pollInterval := 300
	if cfg.Daemon.PollInterval.Seconds >= 90 {
		pollInterval = cfg.Daemon.PollInterval.Seconds
	} else if cfg.Daemon.PollInterval.Seconds < 90 {
		log.Println("Poll interval too small. Falling back to default.")
	} else {
		log.Println("No poll interval was defined. Falling back to default.")
	}
	log.Printf("Poll interval: %ds\n", pollInterval)

	// Create new ticker to perform periodic task.
	ticker := time.NewTicker(time.Duration(pollInterval) * time.Second)

	// Run daemon initially.
	err = Daemon()
	if err != nil {
		return err
	}

	// Run at each ticker iteration.
	for range ticker.C {
		err = Daemon()
		if err != nil {
			return err
		}
	}

	return nil
}
