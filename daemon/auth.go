package daemon

import (
	"errors"
	"os"
)

// IsRoot checks if the current user has root privileges.
func IsRoot() error {
	// Check if user has root priviledges. Also works with sudo.
	if os.Geteuid() != 0 {
		return errors.New("This command requires root privileges")
	}

	return nil
}
