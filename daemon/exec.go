package daemon

import (
	"bytes"
	"errors"
	"os/exec"
	"strings"
)

// Exec runs an arbitrary command.
//
// Currently it does not support spaces in quotation marks as part of commands.
func Exec(command string) error {
	// Prepare command for execution.
	commandSegments := strings.Split(command, " ")
	cmd := exec.Command(commandSegments[0], commandSegments[1:]...)

	// Create buffer for stderr.
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Execute command and check for errors.
	if err := cmd.Run(); err != nil {
		return errors.New(stderr.String())
	}

	return nil
}
