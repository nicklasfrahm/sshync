package daemon

import (
	"os"
)

// Uninstall will disable the daemon from running in the background via the system manager.
//
// Currently the only supported system manager is systemd.
func Uninstall() error {
	// Check if user has root privileges.
	if err := IsRoot(); err != nil {
		return err
	}

	// Stop systemd service.
	if err := Exec("systemctl stop sshync"); err != nil {
		return err
	}

	// Disable systemd service on startup.
	if err := Exec("systemctl disable sshync"); err != nil {
		return err
	}

	// Remove systemd service.
	if err := os.Remove("/etc/systemd/system/sshync.service"); err != nil {
		return err
	}

	return nil
}
