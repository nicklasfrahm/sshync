package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/nicklasfrahm/sshync/cli"
)

func main() {
	// Configure logging.
	log.SetFlags(log.LstdFlags | log.LUTC)

	// Run CLI application.
	if err := cli.Run(); err != nil {
		// Print error message prefix.
		fmt.Print("Error: ")

		// Print all errors with a single line break at the end.
		str := err.Error()
		if str[len(str)-1:] == "\n" {
			fmt.Print(str)
		} else {
			fmt.Println(str)
		}

		// Exit with non-zero exit code.
		os.Exit(1)
	}
}
